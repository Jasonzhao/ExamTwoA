import { Component, OnInit } from '@angular/core';
import { HttpService } from './../http.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
@Component({
  selector: 'app-showpoll',
  templateUrl: './showpoll.component.html',
  styleUrls: ['./showpoll.component.css']
})
export class ShowpollComponent implements OnInit {

  constructor(private _httpService: HttpService,
  	private _route: ActivatedRoute,
    private _router: Router) { }

  ngOnInit() {
  	this.passID();
  	this.getSpecificQuestion();
  	this.getallAnswers(this.tempId);
  }
  tempId = "";
  tempObj = {};
  tempArray = [];
  tempArrayTwo =[];
 // passVoteObj = {};
  passID(){
	  	this._route.params.subscribe((params: Params) => {
	  		console.log("show one id is: ",params['id']);
	  		this.tempId = params['id'];
	  	});
  	}
  	getSpecificQuestion(){
  	console.log("may i be getSpecificquestion?");
  	//console.log("what is id now?",this.passID);
  	let observable = this._httpService.getSpecificquestionInService(this.tempId);
    	observable.subscribe(responseData=>{
    		//this.tempAuthors.push(responseData);
    		
    		
    		console.log("the responsedata in getSpecificQuestion is : ",responseData);
    		this.tempObj = responseData;
    		console.log("what is tempObj is : ",this.tempObj);
    		//this.tempArray.push(responseData);
    		//console.log(this.tempArray);
    		//this._router.navigate(['/']);
    	});
  }

  getallAnswers(id){

  	console.log("may i be getallAnswers?");

  	let observable = this._httpService.getAllAnswers(id);
    	observable.subscribe(responseData=>{
    		//this.tempAuthors.push(responseData);
    		this.tempArray = [];
    		
    		console.log("the responsedata in getall Polls is : ",responseData);
    		this.tempArray.push(responseData);
    		console.log("the tempArray in getall Polls is : ",this.tempArray);
    		 this.tempArrayTwo = this.tempArray[0].options;
    		// //sort by desc
    		// this.tempArrayTwo = this.tempArrayTwo.sort((a,b) => {
    		// 	return b.likes-a.likes;
    		// });
    		//console.log(this.tempArray);
    		//this._router.navigate(['/']);
    	});
  
  	}
  	//add votes :
  	addonevoteOne(ansObj){
  		let observable = this._httpService.addOneLikeinService(ansObj);
    	observable.subscribe(responseData=>{
    		//this.tempAuthors.push(responseData);
    		console.log("what is the resposedata in addoneLikes Subscribe",responseData);
    		this.tempArray.push(responseData);
    		console.log("the responsedata in addoneLike is : ",responseData);
    		this.passID();
		  	//this.getSpecificQuestion();
		  	this.getallAnswers(this.tempId);
    		//this._router.navigate(['/question',ansObj._id]);
    	});
  	}
  	addonevoteTwo(ansObj){
  		let observable = this._httpService.addTwoVoteinService(ansObj);
    	observable.subscribe(responseData=>{
    		//this.tempAuthors.push(responseData);
    		console.log("what is the resposedata in addoneLikes Subscribe",responseData);
    		this.tempArray.push(responseData);
    		console.log("the responsedata in addoneLike is : ",responseData);
    		this.passID();
		  	//this.getSpecificQuestion();
		  	this.getallAnswers(this.tempId);
    		//this._router.navigate(['/question',ansObj._id]);
    	});
  	}
  	addonevotethree(ansObj){
  		let observable = this._httpService.addthreeVoteinService(ansObj);
    	observable.subscribe(responseData=>{
    		//this.tempAuthors.push(responseData);
    		console.log("what is the resposedata in addoneLikes Subscribe",responseData);
    		this.tempArray.push(responseData);
    		console.log("the responsedata in addoneLike is : ",responseData);
    		this.passID();
		  	//this.getSpecificQuestion();
		  	this.getallAnswers(this.tempId);
    		//this._router.navigate(['/question',ansObj._id]);
    	});
  	}
  	addonevoteFour(ansObj){
  		console.log("may i be delete funcgtion");
  		let observable = this._httpService.addFourVoteinService(ansObj);
    	observable.subscribe(responseData=>{
    		//this.tempAuthors.push(responseData);
    		console.log("what is the resposedata in addoneLikes Subscribe",responseData);
    		this.tempArray.push(responseData);
    		console.log("the responsedata in addoneLike is : ",responseData);
    		this.passID();
		  	//this.getSpecificQuestion();
		  	this.getallAnswers(this.tempId);
    		//this._router.navigate(['/question',ansObj._id]);
    	});
  	}
}
