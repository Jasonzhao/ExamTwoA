import { Component, OnInit } from '@angular/core';
import { HttpService } from './../http.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
@Component({
  selector: 'app-newpoll',
  templateUrl: './newpoll.component.html',
  styleUrls: ['./newpoll.component.css']
})
export class NewpollComponent implements OnInit {

  constructor(private _httpService: HttpService,
  	private _route: ActivatedRoute,
    private _router: Router) { }

  ngOnInit() {
  }
  tempQuestion = {question:"",optionOne:"",optionTwo:"",optionthree:"",optionFour:""};
  errors = {};
  questionsubmit(){
  	console.log("may i be question submit?");
  	let observable = this._httpService.addQuestion(this.tempQuestion);
    	observable.subscribe((responseData:any)=>{
    		if (responseData.errors != undefined){
    		this.errors = responseData;
    		}else{

    		
	    		console.log("the responsedata in Answersubmit is : ",responseData);
	    		this._router.navigate(['/']);
    		}
    	});
  }
}

