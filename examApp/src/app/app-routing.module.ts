import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { SecondpageComponent } from './secondpage/secondpage.component';
import { NewpollComponent } from './newpoll/newpoll.component';
import { ShowpollComponent } from './showpoll/showpoll.component';
const routes: Routes = [
	{ path: '',component: SecondpageComponent },
	  { path: 'index',component: HomeComponent },
	  { path: 'new_poll',component: NewpollComponent },
	  { path: 'show_poll/:id',component: ShowpollComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


