import { Component, OnInit } from '@angular/core';
import { HttpService } from './../http.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
@Component({
  selector: 'app-secondpage',
  templateUrl: './secondpage.component.html',
  styleUrls: ['./secondpage.component.css']
})
export class SecondpageComponent implements OnInit {

  constructor(private _httpService: HttpService,
  	private _route: ActivatedRoute,
    private _router: Router) { }

  ngOnInit() {
  	this.getAllQuestion();
  }
  tempArray = [];
  passId= "";
  authorInSearchBar = "";
  //tempObj = {};
 getAllQuestion(){
  	console.log("may i be getallquestion?");
  	let observable = this._httpService.getquestionInService();
    	observable.subscribe(responseData=>{
    		//this.tempAuthors.push(responseData);
    		this.tempArray = [];
    		
    		console.log("the responsedata in getallQuestion is : ",responseData);
    		this.tempArray.push(responseData);
    		console.log(this.tempArray);
    		//this._router.navigate(['/']);
    	// 	for(var i = 0; i<this.tempArray[0].length;i++){
    	// 		var date = new Date(this.tempArray[0][i])
    	// }
    	});
  }
  questiondelete(id){
  	let observable = this._httpService.DeleteInService(id);
    	observable.subscribe(responseData=>{
    		//this.tempAuthors.push(responseData);
    		
    		
    		console.log("the responsedata in questiondelete is : ",responseData);
    		this.getAllQuestion();

    	});
  }
  // checkIfInclude(){
	 //    		return this.tempArray[0][1].question.includes(this.authorInSearchBar);
	 //    	}
	 storedafterSearch = [];
  searchsubmit(){
    	console.log("may i be searchsubmit?");
    	//this.tempAuthors = [];
    	var tempObj = [];
    	for(var i = 0; i<this.tempArray[0].length;i++){
    		//console.log("includes works");
	    		if (this.tempArray[0][i].question.includes(this.authorInSearchBar) ){
	    			//console.log("includes works");
	    			tempObj.push( this.tempArray[0][i]);
	    			
	    		}
	    		
	    	}
	    	this.tempArray[0] = [];
	    	this.tempArray[0]=tempObj;
	    	this.storedafterSearch = tempObj;
	    	if (this.authorInSearchBar == ""){
	    		this.getAllQuestion();
	    	}
	    	//this.tempArray[0].filter(this.checkIfInclude);
    }
}
