import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable()
export class HttpService {

  constructor(private _http: HttpClient) { }
loginService(loginObj){
  	console.log("may i be service");
  	return this._http.post('/api/login/',loginObj);
  }
  logoutService(){
  	return this._http.get('/api/logout/');
  }
  addQuestion(quesObj){
  	console.log("may i be addQuestion in service?");
  	return this._http.post('/api/addPoll/',quesObj);
  }
  getquestionInService(){
  	return this._http.get('/api/getQuestions/');
  }
  getSpecificquestionInService(id){
  	console.log("may i be get specific ques in service?",id);
  	return this._http.get('/api/ShowSpecificQues/'+id);
  }
  getAllAnswers(id){
  	console.log("what is the getallanswer id in service: ",id);
  	return this._http.get('/api/getallAnswers/'+id);
  }
  addOneLikeinService(ansObj){

  	return this._http.put('api/addlike/'+ansObj._id,ansObj);
  }
  
  addTwoVoteinService(ansObj){

  	return this._http.put('api/addlikeTwo/'+ansObj._id,ansObj);
  }
  
   addthreeVoteinService(ansObj){

  	return this._http.put('api/addlikethree/'+ansObj._id,ansObj);
  }
   addFourVoteinService(ansObj){

  	return this._http.put('api/addlikeFour/'+ansObj._id,ansObj);
  }
  DeleteInService(id){
  	console.log("may i be delete in service?");
  	return this._http.delete('api/delete/'+id);
  }
}
