webpackJsonp(["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var home_component_1 = __webpack_require__("./src/app/home/home.component.ts");
var secondpage_component_1 = __webpack_require__("./src/app/secondpage/secondpage.component.ts");
var newpoll_component_1 = __webpack_require__("./src/app/newpoll/newpoll.component.ts");
var showpoll_component_1 = __webpack_require__("./src/app/showpoll/showpoll.component.ts");
var routes = [
    { path: '', component: secondpage_component_1.SecondpageComponent },
    { path: 'index', component: home_component_1.HomeComponent },
    { path: 'new_poll', component: newpoll_component_1.NewpollComponent },
    { path: 'show_poll/:id', component: showpoll_component_1.ShowpollComponent },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forRoot(routes)],
            exports: [router_1.RouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
exports.AppRoutingModule = AppRoutingModule;


/***/ }),

/***/ "./src/app/app.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<h1>Exam Two</h1>\n<a (logout)=\"logout()\" [routerLink]=\"['/index']\"> Log Out</a> \n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var http_service_1 = __webpack_require__("./src/app/http.service.ts");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var AppComponent = /** @class */ (function () {
    function AppComponent(_httpService, _route, _router) {
        this._httpService = _httpService;
        this._route = _route;
        this._router = _router;
        this.title = 'app';
    }
    AppComponent.prototype.ngOnInit = function () { };
    AppComponent.prototype.logout = function () {
        var observable = this._httpService.logoutService();
        observable.subscribe(function (responseData) {
            console.log("successfully log out ");
        });
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            template: __webpack_require__("./src/app/app.component.html"),
            styles: [__webpack_require__("./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [http_service_1.HttpService,
            router_1.ActivatedRoute,
            router_1.Router])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;


/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var app_routing_module_1 = __webpack_require__("./src/app/app-routing.module.ts");
var http_service_1 = __webpack_require__("./src/app/http.service.ts");
var http_1 = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var forms_1 = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
var app_component_1 = __webpack_require__("./src/app/app.component.ts");
var home_component_1 = __webpack_require__("./src/app/home/home.component.ts");
var secondpage_component_1 = __webpack_require__("./src/app/secondpage/secondpage.component.ts");
var newpoll_component_1 = __webpack_require__("./src/app/newpoll/newpoll.component.ts");
var showpoll_component_1 = __webpack_require__("./src/app/showpoll/showpoll.component.ts");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                home_component_1.HomeComponent,
                secondpage_component_1.SecondpageComponent,
                newpoll_component_1.NewpollComponent,
                showpoll_component_1.ShowpollComponent
            ],
            imports: [
                platform_browser_1.BrowserModule,
                app_routing_module_1.AppRoutingModule,
                forms_1.FormsModule,
                http_1.HttpClientModule
            ],
            providers: [http_service_1.HttpService],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;


/***/ }),

/***/ "./src/app/home/home.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/home/home.component.html":
/***/ (function(module, exports) {

module.exports = "<h3>log in page</h3>\n\n<h1>Current Polls</h1>\n<form (submit)=\"loginsubmit()\">Name<input type=\"text\" name=\"aa\" [(ngModel)]=\"tempObj.name\"><input type=\"submit\" name=\"ssb\" value=\"Enter\"></form>"

/***/ }),

/***/ "./src/app/home/home.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var http_service_1 = __webpack_require__("./src/app/http.service.ts");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var HomeComponent = /** @class */ (function () {
    function HomeComponent(_httpService, _route, _router) {
        this._httpService = _httpService;
        this._route = _route;
        this._router = _router;
        this.tempObj = { name: "" };
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent.prototype.loginsubmit = function () {
        var _this = this;
        console.log("may i be loginsubmit?");
        var observable = this._httpService.loginService(this.tempObj);
        observable.subscribe(function (responseData) {
            //this.tempAuthors.push(responseData);
            console.log("the responsedata in loginsubmit is : ", responseData);
            _this._router.navigate(['/']);
        });
    };
    HomeComponent = __decorate([
        core_1.Component({
            selector: 'app-home',
            template: __webpack_require__("./src/app/home/home.component.html"),
            styles: [__webpack_require__("./src/app/home/home.component.css")]
        }),
        __metadata("design:paramtypes", [http_service_1.HttpService,
            router_1.ActivatedRoute,
            router_1.Router])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;


/***/ }),

/***/ "./src/app/http.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var http_1 = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var HttpService = /** @class */ (function () {
    function HttpService(_http) {
        this._http = _http;
    }
    HttpService.prototype.loginService = function (loginObj) {
        console.log("may i be service");
        return this._http.post('/api/login/', loginObj);
    };
    HttpService.prototype.logoutService = function () {
        return this._http.get('/api/logout/');
    };
    HttpService.prototype.addQuestion = function (quesObj) {
        console.log("may i be addQuestion in service?");
        return this._http.post('/api/addPoll/', quesObj);
    };
    HttpService.prototype.getquestionInService = function () {
        return this._http.get('/api/getQuestions/');
    };
    HttpService.prototype.getSpecificquestionInService = function (id) {
        console.log("may i be get specific ques in service?", id);
        return this._http.get('/api/ShowSpecificQues/' + id);
    };
    HttpService.prototype.getAllAnswers = function (id) {
        console.log("what is the getallanswer id in service: ", id);
        return this._http.get('/api/getallAnswers/' + id);
    };
    HttpService.prototype.addOneLikeinService = function (ansObj) {
        return this._http.put('api/addlike/' + ansObj._id, ansObj);
    };
    HttpService.prototype.addTwoVoteinService = function (ansObj) {
        return this._http.put('api/addlikeTwo/' + ansObj._id, ansObj);
    };
    HttpService.prototype.addthreeVoteinService = function (ansObj) {
        return this._http.put('api/addlikethree/' + ansObj._id, ansObj);
    };
    HttpService.prototype.addFourVoteinService = function (ansObj) {
        return this._http.put('api/addlikeFour/' + ansObj._id, ansObj);
    };
    HttpService.prototype.DeleteInService = function (id) {
        console.log("may i be delete in service?");
        return this._http.delete('api/delete/' + id);
    };
    HttpService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], HttpService);
    return HttpService;
}());
exports.HttpService = HttpService;


/***/ }),

/***/ "./src/app/newpoll/newpoll.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/newpoll/newpoll.component.html":
/***/ (function(module, exports) {

module.exports = "<br>\n<a [routerLink]=\"['/']\"> Cancel</a> <br>\n<h5>add new poll here:</h5>\n<h5>If input not match validation, errors will show as below:(if all is fine, then nothing appear)</h5>\n<h5>{{errors.message}}</h5>\n<form (submit)=\"questionsubmit()\">\n\tQuestion<input type=\"text\" name=\"aab\" width=\"200\" [(ngModel)]=\"tempQuestion.question\">\n\tOption 1:<input type=\"text\" name=\"aac\" width=\"200\" [(ngModel)]=\"tempQuestion.optionOne\">\n\tOption 2:<input type=\"text\" name=\"aac\" width=\"200\" [(ngModel)]=\"tempQuestion.optionTwo\">\n\tOption 3:<input type=\"text\" name=\"aac\" width=\"200\" [(ngModel)]=\"tempQuestion.optionthree\">\n\tOption 4:<input type=\"text\" name=\"aac\" width=\"200\" [(ngModel)]=\"tempQuestion.optionFour\">\n\t<button>Cancel</button>\n\t<input type=\"submit\" name=\"bbb\" value=\"Create Poll\">\n</form>"

/***/ }),

/***/ "./src/app/newpoll/newpoll.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var http_service_1 = __webpack_require__("./src/app/http.service.ts");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var NewpollComponent = /** @class */ (function () {
    function NewpollComponent(_httpService, _route, _router) {
        this._httpService = _httpService;
        this._route = _route;
        this._router = _router;
        this.tempQuestion = { question: "", optionOne: "", optionTwo: "", optionthree: "", optionFour: "" };
        this.errors = {};
    }
    NewpollComponent.prototype.ngOnInit = function () {
    };
    NewpollComponent.prototype.questionsubmit = function () {
        var _this = this;
        console.log("may i be question submit?");
        var observable = this._httpService.addQuestion(this.tempQuestion);
        observable.subscribe(function (responseData) {
            if (responseData.errors != undefined) {
                _this.errors = responseData;
            }
            else {
                console.log("the responsedata in Answersubmit is : ", responseData);
                _this._router.navigate(['/']);
            }
        });
    };
    NewpollComponent = __decorate([
        core_1.Component({
            selector: 'app-newpoll',
            template: __webpack_require__("./src/app/newpoll/newpoll.component.html"),
            styles: [__webpack_require__("./src/app/newpoll/newpoll.component.css")]
        }),
        __metadata("design:paramtypes", [http_service_1.HttpService,
            router_1.ActivatedRoute,
            router_1.Router])
    ], NewpollComponent);
    return NewpollComponent;
}());
exports.NewpollComponent = NewpollComponent;


/***/ }),

/***/ "./src/app/secondpage/secondpage.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/secondpage/secondpage.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  HomePage works!\n</p>\n\n<a [routerLink]=\"['/new_poll']\"> Create a new Poll</a>\n<br>\n<br>\n<h5>Search Bar: </h5>\n<form (keyup)=\"searchsubmit()\" ><input type=\"text\" name=\"ssa\" value=\"search\" [(ngModel)]=\"authorInSearchBar\"></form>\n<br>\n<br>\n<div *ngFor=\"let ques of tempArray[0]\"><div>Name: {{ques.name}}</div>--<a [routerLink]=\"['/show_poll',ques._id]\">Question: {{ques.question}}</a>--<div>Date Post: {{ques.createdAt}}</div><form (submit)=\"questiondelete(ques._id)\"><input type=\"submit\" name=\"Answer\" value=\"Delete\"></form></div> "

/***/ }),

/***/ "./src/app/secondpage/secondpage.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var http_service_1 = __webpack_require__("./src/app/http.service.ts");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var SecondpageComponent = /** @class */ (function () {
    function SecondpageComponent(_httpService, _route, _router) {
        this._httpService = _httpService;
        this._route = _route;
        this._router = _router;
        this.tempArray = [];
        this.passId = "";
        this.authorInSearchBar = "";
        // checkIfInclude(){
        //    		return this.tempArray[0][1].question.includes(this.authorInSearchBar);
        //    	}
        this.storedafterSearch = [];
    }
    SecondpageComponent.prototype.ngOnInit = function () {
        this.getAllQuestion();
    };
    //tempObj = {};
    SecondpageComponent.prototype.getAllQuestion = function () {
        var _this = this;
        console.log("may i be getallquestion?");
        var observable = this._httpService.getquestionInService();
        observable.subscribe(function (responseData) {
            //this.tempAuthors.push(responseData);
            _this.tempArray = [];
            console.log("the responsedata in getallQuestion is : ", responseData);
            _this.tempArray.push(responseData);
            console.log(_this.tempArray);
            //this._router.navigate(['/']);
            // 	for(var i = 0; i<this.tempArray[0].length;i++){
            // 		var date = new Date(this.tempArray[0][i])
            // }
        });
    };
    SecondpageComponent.prototype.questiondelete = function (id) {
        var _this = this;
        var observable = this._httpService.DeleteInService(id);
        observable.subscribe(function (responseData) {
            //this.tempAuthors.push(responseData);
            console.log("the responsedata in questiondelete is : ", responseData);
            _this.getAllQuestion();
        });
    };
    SecondpageComponent.prototype.searchsubmit = function () {
        console.log("may i be searchsubmit?");
        //this.tempAuthors = [];
        var tempObj = [];
        for (var i = 0; i < this.tempArray[0].length; i++) {
            //console.log("includes works");
            if (this.tempArray[0][i].question.includes(this.authorInSearchBar)) {
                //console.log("includes works");
                tempObj.push(this.tempArray[0][i]);
            }
        }
        this.tempArray[0] = [];
        this.tempArray[0] = tempObj;
        this.storedafterSearch = tempObj;
        if (this.authorInSearchBar == "") {
            this.getAllQuestion();
        }
        //this.tempArray[0].filter(this.checkIfInclude);
    };
    SecondpageComponent = __decorate([
        core_1.Component({
            selector: 'app-secondpage',
            template: __webpack_require__("./src/app/secondpage/secondpage.component.html"),
            styles: [__webpack_require__("./src/app/secondpage/secondpage.component.css")]
        }),
        __metadata("design:paramtypes", [http_service_1.HttpService,
            router_1.ActivatedRoute,
            router_1.Router])
    ], SecondpageComponent);
    return SecondpageComponent;
}());
exports.SecondpageComponent = SecondpageComponent;


/***/ }),

/***/ "./src/app/showpoll/showpoll.component.css":
/***/ (function(module, exports) {

module.exports = "#vote{\n\twidth: 50px;\n\theight: 30px;\n}"

/***/ }),

/***/ "./src/app/showpoll/showpoll.component.html":
/***/ (function(module, exports) {

module.exports = "<a [routerLink]=\"['/']\"> Go to Polls</a> \n<h3>{{tempObj.question}}</h3>\n\n<p>Options: -- Current Num Of Votes: -- Action:(click button to vote) </p>\n<h3 *ngFor=\"let ans of tempArrayTwo\">  Options: {{ans.optionsOne}}  votes: {{ans.votesOne}} <button id=\"vote\" (click)=\"addonevoteOne(ans)\" value=\"Vote\"></button></h3>\n<h3 *ngFor=\"let ans of tempArrayTwo\">  Options: {{ans.optionsTwo}}  votes: {{ans.votesTwo}} <button id=\"vote\" (click)=\"addonevoteTwo(ans)\" value=\"Vote\"></button></h3>\n<h3 *ngFor=\"let ans of tempArrayTwo\">  Options: {{ans.optionsthree}}  votes: {{ans.votesthree}} <button id=\"vote\" (click)=\"addonevotethree(ans)\" value=\"Vote\"></button></h3>\n<h3 *ngFor=\"let ans of tempArrayTwo\">  Options: {{ans.optionsFour}}  votes: {{ans.votesFour}} <button id=\"vote\" (click)=\"addonevoteFour(ans)\" value=\"Vote\"></button></h3>"

/***/ }),

/***/ "./src/app/showpoll/showpoll.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var http_service_1 = __webpack_require__("./src/app/http.service.ts");
var router_1 = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var ShowpollComponent = /** @class */ (function () {
    function ShowpollComponent(_httpService, _route, _router) {
        this._httpService = _httpService;
        this._route = _route;
        this._router = _router;
        this.tempId = "";
        this.tempObj = {};
        this.tempArray = [];
        this.tempArrayTwo = [];
    }
    ShowpollComponent.prototype.ngOnInit = function () {
        this.passID();
        this.getSpecificQuestion();
        this.getallAnswers(this.tempId);
    };
    // passVoteObj = {};
    ShowpollComponent.prototype.passID = function () {
        var _this = this;
        this._route.params.subscribe(function (params) {
            console.log("show one id is: ", params['id']);
            _this.tempId = params['id'];
        });
    };
    ShowpollComponent.prototype.getSpecificQuestion = function () {
        var _this = this;
        console.log("may i be getSpecificquestion?");
        //console.log("what is id now?",this.passID);
        var observable = this._httpService.getSpecificquestionInService(this.tempId);
        observable.subscribe(function (responseData) {
            //this.tempAuthors.push(responseData);
            console.log("the responsedata in getSpecificQuestion is : ", responseData);
            _this.tempObj = responseData;
            console.log("what is tempObj is : ", _this.tempObj);
            //this.tempArray.push(responseData);
            //console.log(this.tempArray);
            //this._router.navigate(['/']);
        });
    };
    ShowpollComponent.prototype.getallAnswers = function (id) {
        var _this = this;
        console.log("may i be getallAnswers?");
        var observable = this._httpService.getAllAnswers(id);
        observable.subscribe(function (responseData) {
            //this.tempAuthors.push(responseData);
            _this.tempArray = [];
            console.log("the responsedata in getall Polls is : ", responseData);
            _this.tempArray.push(responseData);
            console.log("the tempArray in getall Polls is : ", _this.tempArray);
            _this.tempArrayTwo = _this.tempArray[0].options;
            // //sort by desc
            // this.tempArrayTwo = this.tempArrayTwo.sort((a,b) => {
            // 	return b.likes-a.likes;
            // });
            //console.log(this.tempArray);
            //this._router.navigate(['/']);
        });
    };
    //add votes :
    ShowpollComponent.prototype.addonevoteOne = function (ansObj) {
        var _this = this;
        var observable = this._httpService.addOneLikeinService(ansObj);
        observable.subscribe(function (responseData) {
            //this.tempAuthors.push(responseData);
            console.log("what is the resposedata in addoneLikes Subscribe", responseData);
            _this.tempArray.push(responseData);
            console.log("the responsedata in addoneLike is : ", responseData);
            _this.passID();
            //this.getSpecificQuestion();
            _this.getallAnswers(_this.tempId);
            //this._router.navigate(['/question',ansObj._id]);
        });
    };
    ShowpollComponent.prototype.addonevoteTwo = function (ansObj) {
        var _this = this;
        var observable = this._httpService.addTwoVoteinService(ansObj);
        observable.subscribe(function (responseData) {
            //this.tempAuthors.push(responseData);
            console.log("what is the resposedata in addoneLikes Subscribe", responseData);
            _this.tempArray.push(responseData);
            console.log("the responsedata in addoneLike is : ", responseData);
            _this.passID();
            //this.getSpecificQuestion();
            _this.getallAnswers(_this.tempId);
            //this._router.navigate(['/question',ansObj._id]);
        });
    };
    ShowpollComponent.prototype.addonevotethree = function (ansObj) {
        var _this = this;
        var observable = this._httpService.addthreeVoteinService(ansObj);
        observable.subscribe(function (responseData) {
            //this.tempAuthors.push(responseData);
            console.log("what is the resposedata in addoneLikes Subscribe", responseData);
            _this.tempArray.push(responseData);
            console.log("the responsedata in addoneLike is : ", responseData);
            _this.passID();
            //this.getSpecificQuestion();
            _this.getallAnswers(_this.tempId);
            //this._router.navigate(['/question',ansObj._id]);
        });
    };
    ShowpollComponent.prototype.addonevoteFour = function (ansObj) {
        var _this = this;
        console.log("may i be delete funcgtion");
        var observable = this._httpService.addFourVoteinService(ansObj);
        observable.subscribe(function (responseData) {
            //this.tempAuthors.push(responseData);
            console.log("what is the resposedata in addoneLikes Subscribe", responseData);
            _this.tempArray.push(responseData);
            console.log("the responsedata in addoneLike is : ", responseData);
            _this.passID();
            //this.getSpecificQuestion();
            _this.getallAnswers(_this.tempId);
            //this._router.navigate(['/question',ansObj._id]);
        });
    };
    ShowpollComponent = __decorate([
        core_1.Component({
            selector: 'app-showpoll',
            template: __webpack_require__("./src/app/showpoll/showpoll.component.html"),
            styles: [__webpack_require__("./src/app/showpoll/showpoll.component.css")]
        }),
        __metadata("design:paramtypes", [http_service_1.HttpService,
            router_1.ActivatedRoute,
            router_1.Router])
    ], ShowpollComponent);
    return ShowpollComponent;
}());
exports.ShowpollComponent = ShowpollComponent;


/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
Object.defineProperty(exports, "__esModule", { value: true });
exports.environment = {
    production: false
};


/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var platform_browser_dynamic_1 = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
var app_module_1 = __webpack_require__("./src/app/app.module.ts");
var environment_1 = __webpack_require__("./src/environments/environment.ts");
if (environment_1.environment.production) {
    core_1.enableProdMode();
}
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(app_module_1.AppModule)
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map