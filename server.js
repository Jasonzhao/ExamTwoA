var express = require('express');
// Create an Express App
var app = express();
// Require body-parser (to receive post data from clients)
var bodyParser = require('body-parser');
// Integrate body-parser with our App
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json()); 

// Require path
var path = require('path');
// Setting our Static Folder Directory
app.use(express.static( __dirname + '/ExamApp/dist' ));

 var session = require('express-session');
app.use(session({secret:"SuperSecret"}));
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/basic_mongoose');

//
var Schema = mongoose.Schema;
var QuestionInTwoSchema = new mongoose.Schema({
 question: {type:String,required:true,minlength:8},
 name:{type:String},
 
 
 //text: { type: String }, 
 options: [{type: Schema.Types.ObjectId, ref: 'Option'}],
optionContent:[{ type: String}],
 //NameWhoCommnet:[{type: String}]
 
},{timestamps: true})
mongoose.model('QuestionInTwo', QuestionInTwoSchema); // We are setting this Schema in our Models as 'User'
var questionInTwo = mongoose.model('QuestionInTwo');

//user
var UserSchema = new mongoose.Schema({
 UserName: {type:String},
 
 
},{timestamps: true})
mongoose.model('User', UserSchema); // We are setting this Schema in our Models as 'User'
var user = mongoose.model('User');
//One to many schema

var OptionSchema = new mongoose.Schema({
 // since this is a reference to a different document, the _ is the naming convention!
 _options: {type: Schema.Types.ObjectId, ref: 'QuestionInTwo'},
 optionsOne: { type: String,required:true,minlength:3},
 optionsTwo: { type: String,required:true,minlength:3},
 optionsthree: { type: String,required:true,minlength:3},
 optionsFour: { type: String,required:true,minlength:3},
 votesthree:{type:Number,default:0},
 votesTwo:{type:Number,default:0},
 votesOne:{type:Number,default:0},
 votesFour:{type:Number,default:0},
}, {timestamps: true });
mongoose.model('Option', OptionSchema); // We are setting this Schema in our Models as 'User'
var option = mongoose.model('Option');

//login
app.post('/api/login', function(req, res){

  	console.log("may i be server?");
  	user.findOne({UserName: req.body.name},function(err,results){
  		if(err){
  			console.log('Validation error');
  			res.json(err);
  		}else if(results == null){
  			console.log('no user found');
  			var newUser = new user({UserName:req.body.name});
  			newUser.save(function(err) {
  
			    if(err) {
			      console.log('something went wrong');
			      res.json(err);
			      // res.json({message: "error exist", data: {}});
			    } else { 
			      
			      console.log('successfully added a user!');
			      req.session.userName = req.body.name;
			      res.json(results);

			 	//res.json(PersonArray);
			 	
				}
			})	 
  		}
  		else{
  			console.log('no add, only log in!');
		      req.session.userName = req.body.name;
		      res.json(results);
  		}
  	})
  	// req.session.userId = 1 ;
  	// console.log("test session.userId is : ",req.session.userId);
}) 
//logout    
app.get('/api/logout', function(req, res){
	console.log("may i be logout server? ");
	req.session.userName = "";
  
  
})

//add poll
app.post('/api/addPoll', function(req, res){

  	console.log("may i be server to add poll?",req.body);

  	    var newQuestion = new questionInTwo({question:req.body.question});
        // data from form on the front end
        //console.log("what is post",post);
        newQuestion.name = req.session.userName;
        
        var NewPoll = new option();
        console.log("what is req.body.three",req.body.optionsthree);
        NewPoll.optionsOne = req.body.optionOne;
        NewPoll.optionsTwo = req.body.optionTwo;
        NewPoll.optionsthree = req.body.optionthree;
       NewPoll.optionsFour = req.body.optionFour;
        NewPoll.votesOne = 0;
        NewPoll.votesTwo = 0;
        NewPoll.votesthree = 0;
        NewPoll.votesFour = 0;
      
        console.log("what is NewPoll",NewPoll);
        
      
        // now save both to the DB
        NewPoll.save(function(err){
        	newQuestion.options.push(NewPoll);
        	// newQuestion.options.push(NewPollTwo);
        	// newQuestion.options.push(NewPollThree);
        	// newQuestion.options.push(NewPollFour);
        
	               newQuestion.save(function(errtwo){
	               	if(errtwo){
	               		console.log('ErrorTwo');
	                        res.json(errtwo);
	               	}else{
	               	if(err) {
	                          console.log('Error');
	                        res.json(err);
	                     }else{
	                     		
			                     	
			                     	console.log("save pollone successfully!");
			                          res.json(newQuestion);
			                     }
			                } 
			               })
	            		
            	});
         
	            		
            	
         
    })

app.get('/api/getQuestions', function(req, res){

  	console.log("may i be server to get all question?");
  	questionInTwo.find({}).sort('-name').exec(function(err,results){
 		   if(err) {
      console.log('something went wrong');
      res.json(err);
     
    } else { 
      
	      console.log('successfully find all questions!');
	      res.json(results);

 	
 	
	}
 	})
	
  
})
//show specific question in showpoll page:


app.get('/api/ShowSpecificQues/:_id', function(req, res){
	console.log("what is req.body._id: ",req.params._id);
  questionInTwo.findOne({_id:req.params._id}, function(err, results) {
         if(err) {
      console.log('something went wrong');
      res.json(err);
      // res.json({message: "error exist", data: {}});
    } else { 
      
      console.log('successfully find Specific question!');
      res.json(results);

  //res.json(PersonArray);
  
  }
      })
  //res.json({message: "Success", data: {}});
})

app.get('/api/getallAnswers/:_id', function(req, res){

  	console.log("may i be server to get all Answers?");
 
	console.log("test one!!! ", req.params)
   questionInTwo.findOne({_id:req.params._id})
   .populate('options')
   .exec(function(err, post) {
 		console.log("post before render to index: "+post);
 		res.json(post);
	});
  })
//add votes:
app.put('/api/addlike/:_id',function(req,res){
	// console.log("may i be addlike in server ?");
	// console.log("what is the id pass to addlike: ",req.body._id);
	option.findOne({_id:req.body._id}, function(err, results) {
		console.log("what the results is: ",results);
         if(err) {
      console.log('something went wrong');
      res.json(err);
     
    } else { 
      	results.votesOne += 1;
      	results.save(function(err){
      		if(err){
      			console.log('validation errors');
      			res.json(err);
      		}else{
      			//console.log("sfdsfdsfds",results);
      			res.json(results);
      		}
      	})

 	
 	
		}
    })
	
	

})

app.put('/api/addlikeTwo/:_id',function(req,res){
	// console.log("may i be addlike in server ?");
	// console.log("what is the id pass to addlike: ",req.body._id);
	option.findOne({_id:req.body._id}, function(err, results) {
		console.log("what the results is: ",results);
         if(err) {
      console.log('something went wrong');
      res.json(err);
     
    } else { 
      	results.votesTwo += 1;
      	results.save(function(err){
      		if(err){
      			console.log('validation errors');
      			res.json(err);
      		}else{
      			//console.log("sfdsfdsfds",results);
      			res.json(results);
      		}
      	})

 	
 	
		}
    })
	
	

})
app.put('/api/addlikethree/:_id',function(req,res){
	// console.log("may i be addlike in server ?");
	// console.log("what is the id pass to addlike: ",req.body._id);
	option.findOne({_id:req.body._id}, function(err, results) {
		console.log("what the results is: ",results);
         if(err) {
      console.log('something went wrong');
      res.json(err);
     
    } else { 
      	results.votesthree += 1;
      	results.save(function(err){
      		if(err){
      			console.log('validation errors');
      			res.json(err);
      		}else{
      			//console.log("sfdsfdsfds",results);
      			res.json(results);
      		}
      	})

 	
 	
		}
    })
	
	

})
app.put('/api/addlikeFour/:_id',function(req,res){
	// console.log("may i be addlike in server ?");
	// console.log("what is the id pass to addlike: ",req.body._id);
	option.findOne({_id:req.body._id}, function(err, results) {
		console.log("what the results is: ",results);
         if(err) {
      console.log('something went wrong');
      res.json(err);
     
    } else { 
      	results.votesFour += 1;
      	results.save(function(err){
      		if(err){
      			console.log('validation errors');
      			res.json(err);
      		}else{
      			//console.log("sfdsfdsfds",results);
      			res.json(results);
      		}
      	})

 	
 	
		}
    })
	
	

})
//
//delete
app.delete('/api/delete/:_id', function(req, res){
	console.log("may i be delete in server?");
	console.log("what the req.params._id is: ",req.params._id);
	questionInTwo.findOne({_id:req.params._id}, function(err, results) {
         if(err) {
      console.log('something went wrong');
      res.json(err);
      // res.json({message: "error exist", data: {}});
    } else { 
    	if (results.name==req.session.userName){
			    		questionInTwo.remove({_id:req.params._id}, function(errtwo){
					   if(errtwo) {
						      console.log('something went wrong');
						      res.json(errtwo);
			      
			    	} else { 
			      			console.log('delete work');
			     		res.json({message:'deleted'});
					}
				})
    	}else{
      	
	      console.log('You are not the One create this question,so you cant Delete It!');
	      res.json(results);
      	}
  //res.json(PersonArray);
  
  }
      })
	//
  
  
})     

app.all("*", (req,res,next) => {
  res.sendFile(path.resolve("./ExamApp/dist/index.html"))
})

app.listen(8000, function() {
    console.log("listening on port 8000");
})